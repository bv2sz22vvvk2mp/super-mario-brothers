// Import our Compositor.
import Compositor from "./Compositor.js";

// Import our Matrix.
import { Matrix } from "./math.js";

// Import the TileCollider Class.
import TileCollider from "./TileCollider.js";


export default class Level {
  
  // Default Constructor.
  constructor() {
    // Set our Gravity Constant.
    this.gravity = 2000;

    // Holds the Compositor for the Level. This basically holds all the layers and draws them
    // when we want to by calling the draw method.
    this.compositor = new Compositor();

    // Holds a set of all the Entities in the Level. We load the Entities into this set from
    // the JSON file.
    this.entities = new Set();

    // Holds the Matrix for the Level. Our Main Source of Truth for Game Tiles.
    this.tiles = new Matrix();

    // Holds the TileCollider for the Level. Does things. Matches Tiles Right Now. Thats it. :)
    this.tileCollider = new TileCollider(this.tiles);
  };

  // Update Method. We eventylly feed this to the Traits do do things for any of the Entities.
  // But like here is where we kick it off because......
  update(deltaTime) {

    // Update all the Entities in the Level.
    this.entities.forEach( (entity) => {
      // Update the Entity.
      entity.update(deltaTime);

      // Update and Check the Entity's X-Axis Collision.
      entity.entityPosition.x += entity.entityVelocity.x * deltaTime;
      this.tileCollider.checkX(entity);

      // Update and Check the Entity's Y-Axis Collision.
      entity.entityPosition.y += entity.entityVelocity.y * deltaTime;
      this.tileCollider.checkY(entity);

      entity.entityVelocity.y += this.gravity * deltaTime; // Simulate Gravity by changing the Velocity over time.
    });
  };

};
