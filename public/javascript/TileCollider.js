
// Import the TileResolver.
import TileResolver from "./TileResolver.js";

export default class TileCollider {
  // Default Constructor.
  constructor(tileMatrix) {
    this.tiles = new TileResolver(tileMatrix);
  }

  // Method to check for an Entity's collision with a Tile in the Y-Axis direction.
  checkY(entity) {

    // Collision Detection Optimization. Y here, Y there, Everywhere a YY.
    let y;
    if ( entity.entityVelocity.y > 0 ) {
      y = entity.entityPosition.y + entity.entitySize.y;
    } else if ( entity.entityVelocity.y < 0 ) {
      y = entity.entityPosition.y;
    }

    // Get a matched tile based on the Entity's position.
    const matchedTiles = this.tiles.searchByRange(
      entity.entityPosition.x,
      entity.entityPosition.x + entity.entitySize.x,
      y, y 
    );

    // Loop through the matched tiles checking for a collision.
    matchedTiles.forEach((matchedTile) => {
      // If the matched tile isnt a 'ground' tile, return. Do nothing.
      if (matchedTile.tile.name !== "ground") {
        return;
      }

      /**
       * Special Note. Mario's Y-Position is the top of his collison box. So we need to correct
       *  for that by adding the Entity's size to the Y-Position. And because of this
       *  we need to subtract the Entity's size when setting the actual collision.
       *
       */

      // If the Entity is moving in the upward direction.
      if (entity.entityVelocity.y > 0) {
        // If the Entity is above the Matched Tile, Do things.
        if (entity.entityPosition.y + entity.entitySize.y > matchedTile.y1) {
          entity.entityPosition.y = matchedTile.y1 - entity.entitySize.y;
          entity.entityVelocity.y = 0;
        }
      } else if (entity.entityVelocity.y < 0) {
        if (entity.entityPosition.y < matchedTile.y2) {
          entity.entityPosition.y = matchedTile.y2;
          entity.entityVelocity.y = 0;
        }
      }
    });
  }

  // Method to check for an Entity's collision with a Tile in the X-Axis direction.
  checkX( entity ) {

    // Collision Detection Optimization.
    let x;
    if ( entity.entityVelocity.x > 0 ) {
      x = entity.entityPosition.x + entity.entitySize.x;
    } else if ( entity.entityVelocity.x < 0 ) {
      x = entity.entityPosition.x;
    }

    // Get a matched tile based on the Entity's position.
    const matchedTiles = this.tiles.searchByRange(
      x, x,
      entity.entityPosition.y, entity.entityPosition.y + entity.entitySize.y
    );

    // Loop through the matched tiles checking for a collision.
    matchedTiles.forEach((matchedTile) => {
      // If the matched tile isnt a 'ground' tile, return. Do nothing.
      if (matchedTile.tile.name !== "ground") {
        return;
      }

      // If the Entity is moving in the Right direction.
      if (entity.entityVelocity.x > 0) {
        // If the Entity is above the Matched Tile, Do things.
        if (entity.entityPosition.x + entity.entitySize.x > matchedTile.x1) {
          entity.entityPosition.x = matchedTile.x1 - entity.entitySize.x;
          entity.entityVelocity.x = 0;
        }
      } else if (entity.entityVelocity.x < 0) {
        if (entity.entityPosition.x < matchedTile.x2) {
          entity.entityPosition.x = matchedTile.x2;
          entity.entityVelocity.x = 0;
        }
      }
    });
  }

  // Test Method that takes in an Entity and checks to see what collides with it.
  /*test(entity) {
    // Check for a Collision in the Y-Axis.
    this.checkY(entity);
  }*/
};


