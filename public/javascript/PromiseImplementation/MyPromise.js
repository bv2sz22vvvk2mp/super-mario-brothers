
const STATE = {
  PENDING: "pending",
  FULFILLED: "fulfilled",
  REJECTED: "rejected",
};

export default class MyPromise {
  // The then() CallbackStore. Private.
  #thenCbs = [];
  // The catch() CallbackStore. Private.
  #catchCbs = [];
  // The Current State. Private. Pending by default.
  #state = STATE.PENDING;
  // The Return Value. Private.
  #value = null;

  // We need to properly bind the methods to the class.
  // https://www.javascripttutorial.net/javascript-bind/
  // The bind() method allows an object to borrow a method from another
  // object without making a copy of that method. This is known as
  // function borrowing in JavaScript.
  // Were properly setting the this keyword to the class.
  // I dont really undersandthis yet but hey.
  // This is so that we can handle chaining and we have to set the proper
  // this keyword.
  #onSuccessBind = this.#onSuccess.bind(this);
  #onFailBind = this.#onFail.bind(this);

  // Default Constructor.
  constructor(cb) {
    try {
      /**
       *  The first thing we need to do is call the callback function passing it two different
       *    functions. One for success and one for failure.
       *
       *  onSuccess and onFailure are both Private Methods.
       */
      cb(this.#onSuccessBind, this.#onFailBind);
    } catch (error) {
      /**
       *  If we have an error at any point then we simply call the onFailure method with the error.
       *
       *  onFailure is a Private Method.
       */
      this.#onFail(error);
    }
  }

  /**
   *
   *
   */
  #runCallbacks() {
    // Run the .then() Callbacks.
    if (this.#state === STATE.FULFILLED) {
      this.#thenCbs.forEach((individualCallback) => {
        // Call the Callback with the saved value.
        individualCallback(this.#value);
      });

      this.#thenCbs = []; // Clear the CallbackStore.
    }

    // Run the .catch() Callbacks.
    if (this.#state === STATE.REJECTED) {
      this.#catchCbs.forEach((individualCallback) => {
        // Call the Callback with the saved value. Most likely an error.
        individualCallback(this.#value);
      });

      this.#catchCbs = []; // Clear the CallbackStore.
    }
  }

  // onSuccess() Method. Private.
  #onSuccess(value) {
    // We want the onSuccess() method to be called asynchronously but before
    // going back to the main event loop. Thats waht queueMicrotask() does.
    // Also, a feature of Proimises.
    // https://developer.mozilla.org/en-US/docs/Web/API/HTML_DOM_API/Microtask_guide

    queueMicrotask(() => {
      // If we have RESOLVED or REJECTED then we simply return.
      if (this.#state !== STATE.PENDING) {
        return;
      }

      // Deal if the value is a promise.
      if (value instanceof MyPromise) {
        value.then(this.#onSuccessBind, this.#onFailBind);
        return;
      }

      /**
       * If we have a value then we simply set the value of the promise and set the state to
       *    resolved.
       */
      this.#value = value;
      this.#state = STATE.FULFILLED;
      this.#runCallbacks();
    });
  }

  // onFailure() Method. Private.
  #onFail(value) {
    // We want the onSuccess() method to be called asynchronously but before
    // going back to the main event loop. Thats waht queueMicrotask() does.
    // Also, a feature of Proimises.
    // https://developer.mozilla.org/en-US/docs/Web/API/HTML_DOM_API/Microtask_guide

    queueMicrotask(() => {
      // If we have RESOLVED or REJECTED then we simply return.
      if (this.#state !== STATE.PENDING) {
        return;
      }

      // Deal if the value is a promise.
      if (value instanceof MyPromise) {
        value.then(this.#onSuccessBind, this.#onFailBind);
        return;
      }

      // Check for .catch() Callbacks. We have an error if
      if (this.#catchCbs.length === 0) {
        throw new UncaughtPromiseError(value);
      }

      /**
       * If we have an error then we simply set the error of the promise and set the state to
       *    rejected.
       */
      this.#value = value;
      this.#state = STATE.REJECTED;
      this.#runCallbacks();
    });
  }

  // then() Method. Public.
  then(thenCb, catchCb) {
    /**
     *  Everytime we call the .then() method we simply store the callback in our then() CallbackStore. We are
     *  then going to call the callback in our #onSuccess() method.
     */

    // We want to return a new promise so we can chain the .then() calls.
    return new MyPromise((resolve, reject) => {
      // Always add a Callback.
      this.#thenCbs.push((result) => {
        // This is where we catch anythin. This is because if thenCb is
        //undefined then there is no more chaining. We simply resolve the
        // promise with the result. We move along.
        if (thenCb == null) {
          resolve(result);
          return;
        }

        // We handle the result of the above.
        try {
          resolve(thenCb(result));
        } catch (error) {
          reject(error); // This will catch any errors. This is why we us the try/catch.
        }
      });

      // Same as above but for the catch callbacks.
      this.#catchCbs.push((result) => {
        // This is where we catch anythin. This is because if thenCb is
        //undefined then there is no more chaining. We simply resolve the
        // promise with the result. We move along.
        if (catchCb == null) {
          reject(result); // We reject here.
          return;
        }

        // We handle the result of the above.
        try {
          resolve(catchCb(result));
        } catch (error) {
          reject(error); // This will catch any errors. This is why we us the try/catch.
        }
      });

      // Immediately run the Callback.
      this.#runCallbacks();
    });
  }

  // catch() Method. Public. Same as then() but push to #catchCbs.
  catch(callback) {
    /**
     *  We simply call the .then() method with the success callback as undefined
     *  and the catch callback and let the .then() method handle the rest.
     */
    return this.then(undefined, callback);
  }

  // finally() Method. Public.
  finally(cb) {
    return this.then(
      (result) => {
        cb();
        return result;
      },
      (result) => {
        cb();
        throw result;
      }
    );
  }

  // STATIC METHODS.
  static resolve(value) {
    return new Promise((resolve) => {
      resolve(value);
    });
  }

  static reject(value) {
    return new Promise((resolve, reject) => {
      reject(value);
    });
  }

  static all(promises) {
    const results = [];
    let completedPromises = 0;

    return new MyPromise((resolve, reject) => {
      for (let i = 0; i < promises.length; i++) {
        const promise = promises[i];

        promise
          .then((value) => {
            completedPromises++;
            results[i] = value;
            if (completedPromises === promises.length) {
              resolve(results);
            }
          })
          .catch(reject);
      }
    });
  }

  static allSettled(promises) {
    const results = [];
    let completedPromises = 0;

    return new MyPromise((resolve) => {
      for (let i = 0; i < promises.length; i++) {
        const promise = promises[i];

        promise
          .then((value) => {
            results[i] = { status: STATE.FULFILLED, value };
          })
          .catch((reason) => {
            results[i] = { status: STATE.REJECTED, reason };
          })
          .finally(() => {
            completedPromises++;

            if (completedPromises === promises.length) {
              resolve(results);
            }
          });
      }
    });
  }

  static race(promises) {
    return new MyPromise((resolve, reject) => {
      promises.forEach((promise) => {
        promise.then(resolve).catch(reject);
      });
    });
  }

  static any(promises) {

    const errors = [];
    let rejectedPromises = 0;

    return new MyPromise((resolve, reject) => {
      
      for (let i = 0; i < promises.length; i++) {
        
        const promise = promises[i];
        
        promise.then(resolve).catch((value) => {
          rejectedPromises++;
          errors[i] = value;
          
          if (rejectedPromises === promises.length) {
            reject(new AggregateError(errors, "All promises were rejected"));
          }
        });
      }
    });
  }
};

// The UncoughtMyPromiseError Class.
class UncaughtPromiseError extends Error {
  constructor(error) {
    super(error);

    this.stack = `(in promise) ${error.stack}`;
  };
};

