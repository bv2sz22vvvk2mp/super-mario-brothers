
// Import our Entity Class.
import Entity from "./Entity.js";

// Import our Mario Sprite Loader.
import { loadMarioSprite } from './sprites.js';

// Import the Velocity Trait.
import Velocity from "./traits/Velocity.js";

// Import the Jump Trait.
import Jump from "./traits/Jump.js";

// Import the Go Trait.
import Go from "./traits/Go.js";

// HOF that returns a function to create a Mario Entity. It loads the Mario Sprite first.
function createMario() {

  return loadMarioSprite().then( ( sprite ) => {
    // Create THE new Mario Entity.
    const mario = new Entity();

    // Set Mario's Size.
    mario.entitySize.set(16, 16);

    // Add the Go Trait to Mario.
    mario.addTrait(new Go());

    // Add the Jump Trait to the Mario Entity.
    mario.addTrait(new Jump());

    // Add the Velocity Trait to the Mario Entity.
    //mario.addTrait(new Velocity());

    // Draw Method for Mario. Should do the same thing as createSpriteLayer().
    mario.draw = function drawMario(context) {
      // Draw the Mario Sprite to the Canvas Context w/ the entityPosition. Mario's Position.
      sprite.draw( "idle", context, this.entityPosition.x, this.entityPosition.y );
    };

    // Return the Mario Entity.
    return mario;
  });
};

// Export Our createMario Functions.
export { createMario };
