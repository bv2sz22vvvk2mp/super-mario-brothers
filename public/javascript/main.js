// Import our Loaders Helper File.
import { loadLevel } from './loaders.js';

// Import createMario Function.
import { createMario } from './entities.js';

// Import our Timer Class.
import Timer from './Timer.js';

// Import MyPromise Implementation.
import MyPromise from './PromiseImplementation/MyPromise.js';

// Import the createCollisionLayer Function.
import { createCollisionLayer } from './layers.js';

// Import setupKeyboard Function.
import { setupKeyboard } from './input.js';

// Main Program. JavaScript is a script. This is why we start in the middle. lol.

// Clear the Console for debugging in Firefox.
console.clear();

// Get the Canvas Element.
const canvas = document.getElementById( 'screen' );

// Get the Canvas Context. Do some background things for debugging purposes.
const context = canvas.getContext( '2d' );
context.fillStyle = 'black';
context.fillRect( 0 , 0, 425, 250 );

/**
 *  This is the Promise Chain that will handle the loading of the Level and Sprites.
 * 
 *  Uses a Custom Promise Implementation from Web Dev Simplified.
 *    Source: https://www.youtube.com/watch?v=1l4wHWQCCIc
 * 
 *  Chained Functions: 
 *    loadBackgroundSprites() - Loads the Sprites from the Image File.
 *    loadLevel() - Loads the Level from the JSON File.
 *    loadMarioSprite() - Loads the Mario Sprite from the Image File.
 * 
 *  Resolved Values:
 *    sprites (Object) - The SpriteSheet Object returned from loadBackgroundSprites().
 *    level (Object) - The Level Object returned from loadLevel().
 *    marioSprite (Object) - The SpriteSheet Object returned from loadMarioSprite().
 *    
 */
MyPromise.all([

  loadLevel( "1-1" ),     // Load the Level.
  createMario(),          // Create Mario.

]).then(([ levelMap, mario ]) => {

  // Comments Bitch. Im actually a little proud of this log. Its so nice. lol. And it works.
  console.log("Level Map, Mario Created via Web Dev Simplified Custom Promise.", {
      levelMap,
      mario,
    });
  
  console.log("Visit Web Dev Simplified at https://www.youtube.com/c/WebDevSimplified");


  // Set Marios Position and Velocity. Mario with velocity is easier to see.
  mario.entityPosition.set(64, 180);                  // Set the Inital Position for Mario.
  //mario.entityVelocity.set(200, -600);              // Set the Inital Velocity for Mario.

  // Push the Collision Layer to the LevelMap for display.
  levelMap.compositor.layers.push(createCollisionLayer(levelMap));

  // Add Mario to the Level.
  levelMap.entities.add(mario);

  const input = setupKeyboard(mario);
  input.listenTo(window);

   // Debugging Tool to move Mario around for convenience.
  /**
   *  We use the UI Events API to add Event Listeners to the canvas that allow us to
   *  move Mario to the location of the Mouse Cursor.
   *
   *  The UI Events API defines a system for handling user interactions such as Mouse
   *  and Keyboard input. It does it all.
   *
   *  The Event Interfaces handle different things such as getting coordinates of the
   *  Mouse Cursor, which buttons are pressed and even if things like the Shift or
   *  either of the Alt Keys are pressed. Aditionally we can even get the number of
   *  the Mouse Button that is being pressed or do Mouse Wheel things.
   *
   *  Source: https://developer.mozilla.org/en-US/docs/Web/API/UI_Events
   *
   * 
   * 
   *  For debugging purposes we will use the mousedown and mousemove Events to move 
   *  Mario to the location of the Mouse Cursor.
   */

  ["mousedown", "mousemove"].forEach((eventName) => {
    // Add an Event Listener to the Canvas Element for each UI Event.
    canvas.addEventListener(eventName, (event) => {

      // If the Left Mouse Button is pressed.
      if (event.buttons === 1) {
        mario.entityVelocity.set(0, 0);

        // Set Mario's Position to the Mouse Cursor's Position.
        mario.entityPosition.set(event.offsetX, event.offsetY);
      }
    });
  });

  // Bind the Keyboard State to the Window.
  input.listenTo( window );

  // Create the Timer Instance.
  const timer = new Timer(1/60);

  // Add the Update Function to the Timer Instance.
  timer.update = function update(deltaTime) {
    
    // Update the Level.
    levelMap.update(deltaTime);

    // Draw the Level to the Canvas Context.
    levelMap.compositor.draw(context);

  }

  // Start the Timer.
  timer.start();

});
