/**
 *  In our Array of Arrays which represents the level, we have a lot of smaller tiles
 *  that contain the information that we use to draw things like the backgrounds and
 *  obstacles. However, when we go to represent them on the screen we cant really use
 *  the small tiles we use to represent them in the level. We need to use larger tiles
 *  that are 16x16 pixels that are more appropriate to the rules of the game.
 *
 *  The TileResolver Class acts as the intrepretr between the small tiles and the larger
 *  tiles.
 *
 */

export default class TileResolver {
  
  // Default Constructor.
  constructor(matrix, tileSize = 16) {
    this.matrix = matrix;
    this.tileSize = tileSize;
  };

  // ToIndex Function.
  toIndex(pos) {
    return Math.floor(pos / this.tileSize);
  };

  // Method That Converts 2 pixel Locations to a Range of Indicies. Works for both X and Y Axis.
  toIndexRange(pixelLocation1, pixelLocation2) {

    // Calculate the Maximum position .
    const maxPosition = Math.ceil(pixelLocation2 / this.tileSize) * this.tileSize;
    const indexRange = [];

    let currentPosition = pixelLocation1;
    do {
      // We always want to push the first one because at minium we will have a range of one Tile.
      indexRange.push(this.toIndex(currentPosition));

      // Increment to the next Tile. Were only checking Tiles. Not Pixels.
      currentPosition += this.tileSize;
    } while (currentPosition < maxPosition);

    // Whole point was to get the range of Tiles. Return it.
    return indexRange;
  };


  // Simply returns a Tile with the given X and Y Indexes
  getByIndex(indexX, indexY) {
    // Get the Tile at indexX, indexY.
    const tile = this.matrix.get(indexX, indexY);

    // If the tile exists, return it. 
    if (tile) {

      // Calculate the Y-Position. The Bit location using the Index Location.
      const y1 = indexY * this.tileSize;                        // The Top of the Tile.
      const y2 = indexY * this.tileSize + this.tileSize;        // The Bottom of the Tile.

      // Calculate the X-Position. The Bit location using the Index Location.
      const x1 = indexX * this.tileSize;                        // The Left of the Tile.
      const x2 = indexX * this.tileSize + this.tileSize;        // The Right of the Tile.

      return { tile, x1, x2, y1, y2 };             // Returns an Object.
    }
  };

  // Wrapper Function that we use to get the Tile by its Index.
  searchByPosition(posX, posY) {

    // Simply return getByIndex.
    return this.getByIndex(this.toIndex(posX), this.toIndex(posY));
  };

  // Method that
  searchByRange(x1, x2, y1, y2) {
    const matches = [];

    // We iterate through the X and Y Ranges.
    this.toIndexRange(x1, x2).forEach(indexX => {
      this.toIndexRange(y1, y2).forEach(indexY => {
        
        // Just check everything and see if theres a match.
        const match = this.getByIndex( indexX, indexY );

        // If the match exists, push it to the matches array.
        if (match) {
          matches.push(match);
        }

      });
    });
    
    return matches;
  }

};

// So we can use the TileResolver Class in the Browser Console. I want to keep this.
window.TileResolver = TileResolver;