/**
 *    Class: Compositor
 *
 *    this.image - The Image Object we want to use to draw the SpriteSheet.
 *    this.layers - The store for all the layers we will eventually be compositing together.
 *
 *    draw(name, context, x, y)     - Draws the Layers to the Context.
 *
 *    Notes:
 * 
 *    The job of the Compositor Class is to combine the different layers. Each layer that we 
 *    push onto the Compositor is actually a Higher Order Function that creates the Background 
 *    layers based on what its loaded from the Level File and the Background Sprites.
 * 
 *    The Compositor Class is uses those HOFs to combine the Background layers with other Layers 
 *    such as the Mario layer, other character layers and the collision layers. It kinda manages
 *    them all and is a central location to draw them all to the Canvas.
 *
 *
 * */
export default class Compositor {
  // Default Constructor.
  constructor() {
    this.layers = []; // Store the layers.
  }

  // Draws the layers to the context.
  draw(context) {

    // Iterate through the layers and draw them to the context.
    this.layers.forEach((layer) => {
      layer(context);             // Each layer is a HOF that draws the SpriteLayer to the context.
    });
  }
}
