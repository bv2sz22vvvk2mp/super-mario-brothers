// Import our KeyboardState Class.
import Keyboard from "./KeyboardState.js";

export function setupKeyboard(entity) {
  // Setup the Keyboard State.
  const input = new Keyboard();

  // Add Keyboard Mappings. Got Rid of Space. Moved to W key. Its better this way.
  input.addMapping(87, (keyState) => {
    //console.log("Key Pressed.", keyState);

    // Simply Check if the Spacebar is pressed.
    if (keyState) {
      entity.jump.start(); // Start the jump.  Not Implemented yet.
    } else {
      entity.jump.cancel(); // End the jump.  Not Implemented yet.
    }
  });

  // Add Keyboard Mappings. A key. Left.
  input.addMapping(65, (keyState) => {
    //console.log("Key Pressed.", keyState);
    entity.go.direction = -keyState;
  });

  // Add Keyboard Mappings. D key. Right.
  input.addMapping(68, (keyState) => {
    //console.log("Key Pressed.", keyState);
    entity.go.direction = keyState;
  });

  return input;
}
