import { Trait } from '../Entity.js';

export default class Go extends Trait {

  // Default Constructor.
  constructor() {

    // Call the Trait Constructor.
    super('go');

    this.direction = 0;
    this.speed = 6000;
  };

  // Update the Entity's Velocity.
  update(entity, deltaTime) {
      entity.entityVelocity.x = this.speed * this.direction * deltaTime;
  };

};