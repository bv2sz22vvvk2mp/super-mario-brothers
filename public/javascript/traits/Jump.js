// Import our Entity Class.
import { Trait } from "../Entity.js";

// Velocity Class. Extends Trait Class.
export default class Jump extends Trait {
  
  // Default Constructor. Extend the Trait Constructor passing in "velocity" as the name.
  constructor() {
    super("jump");

    // We need to limit the jumps duration.
    this.duration = 0.5;

    // We need a velocity to apply to the jump.
    this.velocity = 200;

    // We need to know how long to actually jump for.
    this.engageTime = 0;
  };

  // Start Method to start a jump.
  start() {

    // The time the jump is engaged is primed to be the full duration.
    this.engageTime = this.duration;
  };

  // Calcel Method to cancel a jump.
  cancel() {
    this.engageTime = 0;
  }

  // Update things of the jump.
  update(entity, deltaTime) {
    
    // We want to do things if the jump is engaged. Were going to contiunsuly update. 
    if( this.engageTime > 0) {
      entity.entityVelocity.y = -this.velocity; // Decrease the y Velocity.  
      this.engageTime -= deltaTime; // Decrease the engageTime by the deltaTime.
    }
  };
};
