// Import our Entity Class.
import { Trait } from "../Entity.js";

// Velocity Class. Extends Trait Class.
export default class Velocity extends Trait {

  // Default Constructor. Extend the Trait Constructor passing in "velocity" as the name.
  constructor() {
    super("velocity");
  };

  // Update the Entity's Position. This overrides the update() method in the Trait Class.
  update(entity, deltaTime) {

  };
};
